<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="java.sql.*,healthcare.dbutil.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="/healthcare/bootfiles/bootstrap.min.css">
<script src="/healthcare/bootfiles/jquery-3.1.1.min.js"></script>
<script src="/healthcare/bootfiles/bootstrap.min.js"></script>
<title>Register- Doctor</title>
</head>
<body>
	<%@include file="/html/header.html"%>
	<div class="container">
		<h1 class="well">Doctor Registration</h1>
		<div class="col-lg-12 well">
			<div class="row">
				<form method="post" action="/healthcare/DoctorRegistration" enctype="multipart/form-data">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>DOCTOR ID</label> <input type="text"
									placeholder="Enter user id" name="txtuserid"
									class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>PASSWORD</label> <input type="password"
									placeholder="Choose password " name="txtuserpass"
									class="form-control">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>CONTACT NO</label> <input type="text"
									placeholder="Enter contact no" name="txtcontact"
									class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>GENDER<br></label>
									<br>
									<input type="radio" name="gender" value="male"> Male <input
										type="radio" name="gender" value="female"> Female
							</div>
						</div>

						<div class="row">
							<div class="col-sm-12 form-group">
								<label>NAME</label> <input type="text"
									placeholder="Enter your name" name="txtname"
									class="form-control">
							</div>
							<div class="col-sm-12 form-group">
								<label>EMAIL ID</label> <input type="text"
									placeholder="Enter your email" name="txtemail"
									class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label> EDUCATIONAL QUALIFICATION</label> <input type="text"
									placeholder="Enter highest qualification" name="txtqual"
									class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>EXPERIENCE</label> <input type="text"
									placeholder="year's of experience" name="txtschool"
									class="form-control">
							</div>
							
							<div class="col-sm-12 form-group">
								<label>HOSPITAL/ORGANISATION NAME</label> <input type="text"
									placeholder="Enter your workplace name" name="txthospname"
									class="form-control">
							</div>
							<div class="col-sm-12 form-group">
								<label>SPECIALITY</label> <select name="txtspeciality"
									id="specialityinfo">
									<option value="def">Select Speciality</option>

									<%
										String strsql = "select * from speciality";
										ResultSet rs = CrudOperation.fetchData(strsql);
										while (rs.next()) {
									%>
									<option value="<%=rs.getString("specialityname")%>">
						<%=rs.getString("specialityname")%></option>

									<%
										}
									%>
								</select>
							</div>
							<div class="col-sm-12 form-group">
									<label>ADDRESS</label> <input type="text"
										placeholder="Enter your address" name="txtaddress"
										class="form-control">
							</div>

							</div>
						</div>
					</div>
					<div class="form-group">
						<label>LOCATION</label> <select name="txtloc" id="locationinfo">
							<option value="def">Select Location</option>

							<%
								String strsql1 = "select * from location";
								ResultSet re = CrudOperation.fetchData(strsql1);
								while (re.next()) {
							%>
							<option value="<%=re.getInt("locid")%>">
						<%=re.getString("locname")%></option>

							<%
								}
							%>
						</select>
					</div>
					<div class="form-group">
						<label>OTHER INFORMATION </label>
						<textarea placeholder="want to share more with us" rows="3"
							name="txtinfo" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label>UPLOAD PICTURE</label>
						<div class="form-group">
							<input type="file" name="fl" accept="|image/*">
						</div>
					</div>
					<button type="submit" class="btn btn-lg btn-info">Submit</button>
			</form>
			</div>
		</div>
	</div>



</body>
</html>