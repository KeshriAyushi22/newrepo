package com.ayushi.Util;

import com.ayushi.FL.SolarService;

 interface DaoLayer {

	public void save(Object ob);
	public void update(int id);
	public void delete(Object ob);
	public SolarService fetch(int id);

}
