package com.ayushi.Util;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.ayushi.FL.SolarService;

public  class DaoLayerImpl {  //later check with the implementing dao layer.

	public static void save(Object ob) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session= sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(ob);
		transaction.commit();
		session.close();
	}

	public static Object fetch(int id){
	Object ss=null;
		try{
			SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
			Session session= sessionFactory.openSession();
			Transaction transaction =  session.beginTransaction();
			 ss= (SolarService) session.get(SolarService.class, id);
			 transaction.commit();
				session.close();

		}
		catch(Exception e) {
			System.out.println(e);
		}
		
		return ss;
	}
	
	
	public void update(int id) {
		// TODO Auto-generated method stub

	}
	public static  void delete(Object ob) {
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session= sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.delete(ob);;
		transaction.commit();
		session.close();
	}

}	


