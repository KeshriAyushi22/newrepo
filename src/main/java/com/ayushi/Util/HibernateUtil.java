package com.ayushi.Util;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;   //in hibernate 4.1.2
public class HibernateUtil {

	
	private static SessionFactory sessionFactory = buildSessionFactory(); 

	private static SessionFactory buildSessionFactory() {
		try {
			Configuration configuration = new Configuration();
			configuration.configure("hibernate.cfg.xml");
			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties())
					.buildServiceRegistry();
			SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
			return factory;
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
	  
//use check 
		if(sessionFactory!=null)
		 return sessionFactory;
		else
			buildSessionFactory();
		return null;
	}

	public static void shutdown() {
		getSessionFactory().close();
	}

}



